<?php
 if (isset($_FILES) && count($_FILES) > 0) {
   echo '<p>'.Utils::uploadFile($_FILES['xcl']).'</p>';
 }
?>


<form action="" method="POST" enctype="multipart/form-data">
  <p>Please select excel file by clicking <strong>Choose Excel File</strong> then click <strong>Save</strong> to upload.</p>
  <select name="country">
   <option value="DE">Germany</option>
   <option value="CH">Switzerland</option>
  </select>
  &nbsp; <span style="background-color: rgb(170, 160, 250); cursor: pointer; padding: 4px 8px; border-radius: 2px;" id="upload" onclick="document.getElementById('xcl').click();">Choose Excel File</span>
  <input onchange="selected(this.files);" style="display: none" id="xcl" type="file" name="xcl" />
  &nbsp; <input id="submit" disabled type="submit" style="display: inline !important;" value="Save" />
<script>
  function selected(files) {
    var target = document.getElementById('upload');
    if (files.length > 0) {
      target.textContent = files[0].name;
      document.getElementById('submit').removeAttribute('disabled');
    } else {
      target.textContent = 'Select excel file';
    }
  }
</script>
</form>

<hr />