<!doctype html>
<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width,inital-scale=1.0">
 <link href="public/app.css" rel="stylesheet" />
 <title>Home - City Zip</title>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
</head>
<body>
 <div class="container">
   <div class="row">
     <?php include 'src/views/pages/upload.php';  ?>
   </div>
   <div class="row">
     <?php include 'src/views/pages/show.php'; ?>
   </div>
 </div>
 <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
 <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 <script>
    $(document).ready(function(){
      $('#dataTable').DataTable( {
        ajax: {
          url: 'ajax.php',
          dataSrc: ''
        },
        columns: [
          { data: 'name' },
          { data: 'ortname'},
          { data: 'plz'},
          { data: 'country' }
        ]
      });
    });
 </script>
</body>
</html>
